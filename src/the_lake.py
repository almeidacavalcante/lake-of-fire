import os
import pyautogui, time
from random import randrange
from threading import Thread
from xdo import Xdo
import subprocess, signal

class TheLake:

    pyautogui.FAILSAFE = False

    def move_mouse(self):
        x = randrange(-1500, 1500)
        y = randrange(-1500, 1500)
        duration = randrange(3)
        random_decision = randrange(10)
        if (random_decision > 5):
            pyautogui.moveRel(x, y, duration=duration)


    def change_tab(self):
        pyautogui.keyDown("ctrl")
        time.sleep(randrange(4, 20)/10)
        pyautogui.press("tab")
        time.sleep(randrange(4, 20)/10)
        pyautogui.keyUp("ctrl")

    def change_window(self):
        pyautogui.keyDown("alt")
        time.sleep(randrange(4, 20)/10)
        pyautogui.press("tab")
        time.sleep(randrange(4, 20)/10)
        pyautogui.keyUp("alt")

    def scroll(self):
        distance = randrange(-5, 5)
        pyautogui.scroll(distance)
        time.sleep(randrange(4, 20)/10)

    def shutdownHubstaff(self):
        os.system('wmctrl -F -c "Hubstaff"')

    def clear(self):
        subprocess.call(["xdotool", "keyup", "Tab"])
        subprocess.call(["xdotool", "keyup", "alt"])
        subprocess.call(["xdotool", "keyup", "ctrl"])
        pyautogui.keyUp("alt")
        pyautogui.keyUp("ctrl")

