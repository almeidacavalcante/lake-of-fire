import time
from random import randrange

from the_lake import TheLake

theLake = TheLake()

print("For how many minutes? (tipe an integer)")
minutes = input()

timeout = time.time() + 60 * int(minutes)

start_time = time.time() + 10

while True:
    if time.time() > start_time:
        try:
            theLake.change_tab()
            theLake.move_mouse()
            theLake.scroll()

            theLake.change_window()
            theLake.move_mouse()
            theLake.scroll()

            if time.time() > timeout:
                theLake.clear()
                theLake.shutdownHubstaff()
                break

        except Exception:
            print('EXCEPTION!')
            theLake.clear()
            theLake.shutdownHubstaff()
            break
