#!/bin/bash

move_mouse()
{
  a=$(seq -12 12 | shuf -n 1)
  b=$(seq -12 12 | shuf -n 1)

  for x in $(seq $a $b); do
    for y in $(seq $a $b); do
      xdotool mousemove_relative -- $x $y
    done 
  done 
}

scroll() 
{
  number=$(seq 4 5 | shuf -n 1)
  xdotool click $number
  xdotool click $number
  xdotool click $number
  xdotool click $number
  xdotool click $number
  xdotool click $number
  xdotool click $number
  sleep $(echo "scale=8; $RANDOM/$1 + 1" | bc )
} 

random_keys()
{
  xdotool key ctrl
  xdotool key alt
  xdotool key ctrl
  xdotool key alt
}

change_tab()
{
  xdotool keydown ctrl
  sleep $(echo "scale=8; $RANDOM/$1 + 1" | bc )
  xdotool key Tab
  xdotool key Tab
  sleep $(echo "scale=8; $RANDOM/$1 + 1" | bc )
  xdotool keyup ctrl
}

change_window()
{
  xdotool keydown alt
  sleep $(echo "scale=8; $RANDOM/$1 + 1" | bc )
  xdotool key Tab
  sleep $(echo "scale=8; $RANDOM/$1 + 1" | bc )
  xdotool keyup alt
}

clear()
{
  xdotool keyup alt
  xdotool keyup ctrl
  xdotool keyup Tab
}

close_hubstaff()
{
  wmctrl -F -c "Hubstaff"
}

init()
{
  end_time=$1
  nine_minutes=$(($SECONDS + 1 * 4))
  while [ $SECONDS -lt $end_time ]
  do
    x=$(seq 0 360 | shuf -n 1) 
    y=$(seq 0 360 | shuf -n 1) 

    if [ $SECONDS -lt $nine_minutes ]; then 
      seed=$(seq 30000 50000 | shuf -n 1)
      change_window $seed
      change_tab $seed
      move_mouse $x $y
      scroll $seed

      change_window $seed
      change_tab $seed
      move_mouse $x $y
      scroll $seed

      random_keys
      random_keys
      random_keys
      random_keys

    else
      nine_minutes=$(($SECONDS + 1 * 4))
    fi
  done
  clear 
  close_hubstaff
}
(sleep $1; end=$((SECONDS + $2 * $3)); init $end) & 

