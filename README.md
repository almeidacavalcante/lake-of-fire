                # Instalation

## Dependencies

It depends on: 

```bash
sudo apt-get install python3-tk python3-dev
```

Install virtualenv

```bash
sudo apt install --reinstall python-virtualenv
```

Start a new environment

```bash
virtualenv env -p python3
```

Activate the environment

```bash
source env/bin/activate
```

Install dependencies

```bash
pip install -r dependencies.txt
```

Start the console application

```bash
python src/index.py
```

Enter for how many minutes and that's it!
